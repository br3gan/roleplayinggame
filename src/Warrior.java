
/**
 * 
 * @author racoon
 * @brief Warrior Class.
 */
public class Warrior extends Champion {

	/**
	 * @brief Constructor.
	 * @param name States the Name of the Warrior
	 * @param hp States the Hit Points Pool of the Warrior
	 * @param mana States the Mana Pool of the Warrior
	 * @param attSpeed States the attack speed of the Warrior
	 * @param damage States the Basic Damage of the Warrior
	 * @param criticalStrike States the chance noex Warrior's hit to be critical
	 */
	public Warrior(String name, int hp, int mana, int attSpeed,
			int damage, double criticalStrike) {
		
		super(name, hp, mana, attSpeed, damage, criticalStrike);
		this.skill[0]=true;
		this.skill[1]=true;
		this.skill[2]=false;
	}

	/**
	 * @brief Warrior's Q. Applies percentage damage and stuns enemy for 3 rounds
	 */
	void castQ(Champion other) {	
		int damageDelt = (int)(other.getHp() * 0.1); 
		System.out.println("\t" + getName() + " hits " + other.getName() + " for " + damageDelt + " damage!");
		other.setStatus(Debuff.stun, 3);
		assertDamage(other, damageDelt);
	}
	void castQ(){}
	
	/**
	 * @brief Warrior's W applies damage to enemy equal to 10% of this champion's HP making self to lose 2% of his HP. 
	 */	
	void castW(Champion other) {
		int damageDelt = (int)(getHp() * 0.02);
		System.out.println("\t" + getName() + " hits " + other.getName() + " for " + damageDelt + " damage!");
		assertDamage(other, damageDelt);
	}
	void castW(){}
	
	void castE(Champion other){}
	/**
	 * @brief Warrior regenerates 0.05% of his maximum health
	 */
	void castE() {
		int regeneration = (int)(getMaxHp() * 0.05);
		if ( regeneration + getHp() > getMaxHp() ){
			regeneration = getMaxHp() - getHp();
		}
		System.out.println("\t" + getName() + " regenerated " + regeneration + " hit points!");
		setHp(getHp() + regeneration);
	}
}
