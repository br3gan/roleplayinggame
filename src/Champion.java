
import java.util.*;

/**
 * @author racoon
 * @brief Champion concrete class.
 * Each Champion extends this base class, and overloads its functions here
 */
public abstract class Champion {

	/**
	 * @author racoon
	 * @brief Pool class contains the current amount and the maximum amount of a stat. Used in HP and Mana.
	 */
	public class Pool {
		public final int max; /* max value (initial and constant) */
		public int curr; /* current value */
		public Pool(int max) {
			this.max = max;
			this.curr = max;
		}
	}
	
	public static final int DEBUFF_EFFECTS = 2;
	
	public enum Debuff{
		silence,
		stun
	}
	
	/**
	 * @author racoon
	 * @brief list of all possible debuffs a Champion can have. 
	 * @param on Shows if Champion suffers from any debuff.
	 * @param effects An array showing the remaining rounds of each debuff.  
	 */
	public class Debuffs {
		protected boolean on;
		protected byte effect[];
		
		Debuffs(){
			on=false;
			effect = new byte[DEBUFF_EFFECTS];
		}
	}
	
	/**
	 * @brief userInput Standard object to accept input from user
	 */
	Scanner userInput = new Scanner(System.in);
	
	/// \brief states the number of all the Champions
	public static int numberOfChampions;
	
	/// \brief name States the name of the Champion 
	private String name;
	
	/// \brief States the Health Points of the Champion
	private Pool hp;
	
	/// \brief mana States the mana pool of the Champion
	private Pool mana;

	/// \brief attStpped States the attack speed of the Champion
	private int attSpeed;
	
	/// \brief damage States the basic damage of the champion
	private int damage;

	/// \brief criticalChance
	private double criticalChance;
	
	/// \brief states the current status of the Champion	
	protected Debuffs debuffs;
	
	/// \brief Skill array
	protected boolean skill[] = new boolean[3];
	
	/**
	 * @return name Returns the name of the Champion
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name Stores the name of the Champion
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return Returns the current hit points of the Champion
	 */
	public int getHp() {
		return hp.curr;
	}

	/**
	 * @return The maximum hit points of the Champion
	 */
	public int getMaxHp() {
		return hp.max;
	}
	
	public void setHp(int hp) {
		this.hp.curr = hp;
	}

	public int getMana() {
		return mana.curr;
	}
	
	public int getMaxMana() {
		return mana.max;
	}

	public void setMana(int mana) {
		this.mana.curr = mana;
	}

	/**
	 * @return attSpeed Returns the attack speed of the Champion
	 */
	public int getAttSpeed() {
		return attSpeed;
	}

	/**
	 * @param attSpeed Stores the attach speed of the Champion
	 */
	public void setAttSpeed(int attSpeed) {
		this.attSpeed = attSpeed;
	}

	/**
	 * @return damage Returns the basic damage of the Champion
	 */
	public int getDamage() {
		return damage;
	}

	/**
	 * @param damage Stores the basic damage of the Champion
	 */
	public void setDamage(int damage) {
		this.damage = damage;
	}

	/**
	 * @return damage Returns the basic damage of the Champion
	 */
	public double getCtiricalChance() {
		return criticalChance;
	}

	/**
	 * @param damage Stores the basic damage of the Champion
	 */
	public void setCriticalChance(int criticalChance) {
		this.criticalChance = criticalChance;
	}
	
	/**
	 * @return status Returns the status of the Champion
	 */
	public boolean getStatus() {
		return debuffs.on;
	}

	/**
	 * @param status Stores the status of the Champion
	 */
	public void setStatus(Debuff effect, int numOfRounds) {
		if (!this.debuffs.on){
			this.debuffs.on=true;
		}
		this.debuffs.effect[effect.ordinal()]+=numOfRounds;
	}
	
	public void assertDamage(Champion other, int damageDelt){
		if (other.getHp() >= damageDelt){
			other.setHp(other.getHp() - damageDelt); 
		}
		else{
			other.setHp(0);
		}
	}
	
	/**
	 * @brief Champion Constructor.
	 * @param name States the name of the Champion.
	 * @param hp States the Hit Points pull of the Champion
	 * @param mana States the mana pool of the Champion
	 * @param attSpeed States the attack speed of the champion
	 * @param damage States the basic damage output of the Champion
	 * @param criticalChance States the possibility for the Champion to hit a critical strike 
	 * @param debuffs States the current debuffs the Champion is suffering from
	 */
	public Champion(String name, int hp, int mana, int attSpeed,
			int damage, double criticalChance) {
		this.name = name;
		this.hp = new Pool(hp);
		this.mana = new Pool(mana);
		this.attSpeed = attSpeed;
		this.damage = damage;
		this.criticalChance = criticalChance;
		this.debuffs = new Debuffs();
	}
	
	/**
	 * Skills:
	 * Each Champion has three (3) different skills, active (can affect other champions) or passive (affects only self).
	 */
	
	/// \brief castQ Casts the Q ability of the champion
	/**
	 * Active Skill.
	 * Executes an overloading function. A curtain champion hits another.
	 */
	abstract void castQ(Champion other);
	
	/// \brief castQ Casts the Q ability of the champion
	/** 
	 * Passive Skill.
	 * Executes an overloading function. A curtain champion hits another.
	 */
	abstract void castQ();
	
	/// \brief castW Casts the W ability of the champion
	/** 
	 * Active Skill.
	 * Executes an overloading function. A curtain champion hits another.
	 */
	abstract void castW(Champion other);
	
	/// \brief castQ Casts the Q ability of the champion
	/* 
	 * Passive Skill.
	 * Executes an overloading function. A curtain champion hits another.
	 */
	abstract void castW();
	
	
	/// \brief castE Casts the E ability of the champion
	/** 
	 * Active Skill.
	 * Executes an overloading function. A curtain champion hits another.
	 */
	abstract void castE(Champion other);
	
	/// \brief castE Casts the E ability of the champion
	/** 
	 * Passive Skill.
	 * Executes an overloading function. A curtain champion hits another.
	 */
	abstract void castE();
	
	public void basicAttack(Champion other){
		
		int damageDelt = 0;
		int variance = (int)(Math.random() * (getDamage() * 0.1));
		
		boolean critical = Math.random() < this.getCtiricalChance();
		if (critical){
			damageDelt = this.getDamage();
		}
		
		damageDelt += this.getDamage() - variance;
		assertDamage(other, damageDelt);
		System.out.println("\t" + getName() + " hits " + other.getName() + " for " + damageDelt + " damage!");
		if (critical){
			System.out.println( "\t" + getName() + "'s hit was a critical strike!");
		}
	} 
	
	/// \brief prints current stats of the Champion
	public void printStats(){
		// 'Name' has %d HP and %d mana
		System.out.println(name + " has " + hp.curr + "/" + hp.max + " hp and " + mana.curr + "/" + mana.max + " mana");
	}

	/// \brief Read move 
	public byte readMove(){
		byte move = 'W';
		System.out.println("Give move {Q,W,E,R}");
		if (userInput.hasNextByte()){
			move = userInput.nextByte();
		}
		return move;
	}
	
	public int checkNupdateStatus(){
		
		int debuff = -1; 
		if ( this.debuffs.on ){
			for (int i = DEBUFF_EFFECTS-1 ; i >= 0; i--){
				if (debuffs.effect[i]>0){
					System.out.println("\t" + name + " is " + Debuff.values()[i].name() + "ed for another " + debuffs.effect[i] + " rounds!");
					debuffs.effect[i]-=1;
					debuff = (i>debuff) ? i : debuff;
					break;
				}
			}
		}
		return debuff;
	}
	
	/// \brief Declare Champion's move.
	public void move(byte input, Champion other){
		
		int debuff = checkNupdateStatus();
		byte givenMove;
		
		if (debuff == Debuff.stun.ordinal()){
			return;
		}
		else if(debuff == Debuff.silence.ordinal()){
			givenMove = 'R';
		}
		else{
			givenMove = input;
		}
		
		switch (givenMove){
		case 'Q':
			if (skill[0] == true)
				castQ(other);
			else
				castQ();
			break;
		case 'W':
			if (skill[1] == true)
				castW(other);
			else
				castW();
			break;
		case 'E':
			if (skill[2] == true)
				castE(other);
			else
				castE();
			break;
		default:
			basicAttack(other);
			break;
		}
	}

	public static void main(String args[]){
		Champion champ1 = new Warrior("Conan", 1000, 500, 344, 100, 0.2);
		Champion champ2 = new Warrior("Rocky", 1000, 500, 344, 100, 0.2);
		
		champ1.printStats();
		champ2.printStats();
		
		champ1.move((byte)'Q', champ2);
		champ1.printStats();
		champ2.printStats();
		
		champ2.move((byte)'Q', champ1);
		champ1.printStats();
		champ2.printStats();
		
		champ1.move((byte)'W', champ2);
		champ1.printStats();
		champ2.printStats();
		
		champ2.move((byte)'R', champ1);
		champ1.printStats();
		champ2.printStats();
		
		champ1.move((byte)'W', champ2);
		champ1.printStats();
		champ2.printStats();

		champ2.move((byte)'W', champ1);
		champ1.printStats();
		champ2.printStats();

		champ1.move((byte)'R', champ2);
		champ1.printStats();
		champ2.printStats();
		
		champ2.move((byte)'R', champ1);
		champ1.printStats();
		champ2.printStats();
		
	}
}